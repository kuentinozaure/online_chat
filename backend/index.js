const express = require('express')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server, { origins: '*:*'})
const { v4: uuidV4 } = require('uuid')
var cors = require('cors')
var request = require('request');

const SimpleNodeLogger = require('simple-node-logger'),
    opts = {
        logFilePath:'logs.log',
        timestampFormat:'YYYY-MM-DD HH:mm:ss.SSS'
    },
log = SimpleNodeLogger.createSimpleLogger( opts );

app.set('', '')
app.use(express.static('public'))
app.use(cors())

app.get('/api/room', (req, res) => {
  res.json({'selectedRoom':`${uuidV4()}`})
  
});

const port = process.env.PORT || 3000

io.on('connection', socket => {

  socket.on('joinChat', (roomId, userId) => {
    log.info(userId, ' connected to ', roomId, ' at ', new Date().toJSON());

    socket.join(roomId)

    socket.on('sendMessage', (roomId,userId,message) => {
      log.info(userId, ' has send : ', message, ' in the room ', roomId, ' at ', new Date().toJSON());
      socket.to(roomId).emit('messageIncoming', {
          user : userId,
          room: roomId,
          message: message,
        });
    });

    socket.on('disconnect', () => {

     log.info(userId, ' disconnected to ', roomId, ' at ', new Date().toJSON());
      socket.to(roomId).broadcast.emit('userDisconnected', userId)
    });

  });
})

log.info('Server started at ', new Date().toJSON());
server.listen(port)
