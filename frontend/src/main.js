import Vue from 'vue'
import App from './App.vue'
import VueAxios from 'vue-axios'
import axios from 'axios'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import VueSocketIO from 'vue-socket.io'
import '@mdi/font/css/materialdesignicons.css'
import './registerServiceWorker'


Vue.use(Buefy)

Vue.use(VueAxios, axios)

console.log(process.env)
console.log(process.env.VUE_APP_SOCKET_API)
Vue.use(new VueSocketIO({
    debug: true,
    connection: process.env.VUE_APP_SOCKET_API,
}))

Vue.config.productionTip = false


new Vue({
  render: h => h(App),
}).$mount('#app')
